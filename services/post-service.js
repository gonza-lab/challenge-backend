const db = require('../models');
const error = require('../errors/error');
const { Op } = require('sequelize');

const attributes = ['id', 'title', 'img', 'body', 'createdAt'];

const create = async ({ category, ...post }) => {
  let newPost = await db.Post.create(post);

  if (category) {
    const categoryDB = await db.Category.findOne({
      where: { name: category },
    });

    if (categoryDB) {
      await newPost.setCategory(categoryDB);
    } else {
      await newPost.createCategory({ name: category });
    }
  }

  newPost = await db.Post.findOne({
    where: { id: newPost.id },
    include: [db.Category, { model: db.User, attributes: ['id', 'email'] }],
    attributes: { exclude: ['categoryId'] },
  });

  return newPost.dataValues;
};

const readAll = async () => {
  return await db.Post.findAll({
    include: [db.Category, { model: db.User, attributes: ['id', 'email'] }],
    attributes,
    order: [['createdAt', 'DESC']],
  });
};

const readById = async (id) => {
  const post = await db.Post.findOne({
    include: [db.Category, { model: db.User, attributes: ['id', 'email'] }],
    attributes,
    where: { id },
  });

  if (!post) throw new error.NotFound();

  return post;
};

const updateById = async (id, userId, { category, ...newPost }) => {
  const isUpdated = await db.Post.update(newPost, {
    where: {
      [Op.and]: [{ id }, { userId }],
    },
  });

  if (!isUpdated[0])
    throw new error.BadRequest(
      'El post que intenta actualizar no existe o no le pertenece.'
    );

  let postDB = await db.Post.findOne({
    where: { id },
    attributes,
    include: [db.Category, { model: db.User, attributes: ['id', 'email'] }],
  });

  if (category) {
    const categoryDB = await db.Category.findOne({
      where: { name: category },
    });

    if (categoryDB) {
      await postDB.setCategory(categoryDB);
    } else {
      await postDB.createCategory({ name: category });
    }
  }

  postDB = await db.Post.findOne({
    where: { id },
    attributes,
    include: [db.Category, { model: db.User, attributes: ['id', 'email'] }],
  });

  return postDB.dataValues;
};

const deleteById = async (id, userId) => {
  const isDeleted = await db.Post.destroy({
    where: {
      [Op.and]: [{ id }, { userId }],
    },
  });

  if (!isDeleted)
    throw new error.BadRequest(
      'El post que intenta eliminar no existe o no le pertenece.'
    );
};

module.exports = { create, readAll, readById, updateById, deleteById };
