const { request, response } = require('express');
const post_service = require('../services/post-service');

const create = async (req = request, res = response) => {
  let code = 201;
  let resContent = { ok: true };
  const newPost = req.body;
  const userId = req.user.id;

  try {
    resContent = {
      ...resContent,
      ...(await post_service.create({ ...newPost, userId })),
    };
  } catch (error) {
    console.log(error)
    code = error.code || 500;
    resContent = {
      ok: false,
      msg: error.msg || 'Ha ocurrido un error. Hable con el administrador',
    };
  }

  res.status(code).json(resContent);
};

const readAll = async (req = request, res = response) => {
  let code = 200;
  let resContent = {};

  try {
    resContent = await post_service.readAll();
  } catch (error) {
    code = error.code || 500;
    resContent = {
      ok: false,
      msg: error.msg || 'Ha ocurrido un error. Hable con el administrador',
    };
  }

  res.status(code).json(resContent);
};

const readById = async (req = request, res = response) => {
  let code = 200;
  let resContent = {};

  try {
    resContent = await post_service.readById(req.params.id);
  } catch (error) {
    code = error.code || 500;
    resContent = {
      ok: false,
      msg: error.msg || 'Ha ocurrido un error. Hable con el administrador',
    };
  }

  res.status(code).json(resContent);
};

const updateById = async (req = request, res = response) => {
  let code = 200;
  let resContent = { ok: true };

  try {
    resContent = {
      ...resContent,
      ...(await post_service.updateById(req.params.id, req.user.id, req.body)),
    };
  } catch (error) {
    console.log(error)
    code = error.code || 500;
    resContent = {
      ok: false,
      msg: error.msg || 'Ha ocurrido un error. Hable con el administrador',
    };
  }

  res.status(code).json(resContent);
};

const deleteById = async (req = request, res = response) => {
  let code = 200;
  let resContent = { ok: true };

  try {
    await post_service.deleteById(req.params.id, req.user.id);
  } catch (error) {
    code = error.code || 500;
    resContent = {
      ok: false,
      msg: error.msg || 'Ha ocurrido un error. Hable con el administrador',
    };
  }

  res.status(code).json(resContent);
};

module.exports = { create, readAll, readById, updateById, deleteById };
