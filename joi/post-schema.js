const joi = require('joi');

const create = joi.object({
  title: joi.string().required(),
  body: joi.string().required(),
  img: joi.string().uri().required(),
  category: joi.string(),
});

const update = joi.object({
  title: joi.string(),
  body: joi.string(),
  img: joi.string().uri(),
  category: joi.string(),
});

module.exports = { create, update };
