const { response, request } = require('express');
const jwt = require('jsonwebtoken');
const error = require('../errors/error');

const require_login = (req = request, res = response, next) => {
  const token = req.headers.authorization;

  try {
    if (!token) throw new error.RequireLogin();
    const { id } = jwt.verify(token.split(' ')[1], process.env.SECRET);
    req.user = { id };
    next();
  } catch (error) {
    code = error.code || 500;
    resContent = {
      ok: false,
      error:
        error.message || 'Ha ocurrido un error. Hable con el administrador',
    };

    res.status(code).json(resContent);
  }
};

module.exports = require_login;
