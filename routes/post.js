const { Router } = require('express');
const joi_validator = require('../middlewares/joi-validator');
const post_schema = require('../joi/post-schema');
const post_controller = require('../controllers/post-controller');
const require_login = require('../middlewares/require-login');

const route = Router();

route.get('/', post_controller.readAll);
route.get('/:id', post_controller.readById);
route.post(
  '/',
  require_login,
  joi_validator(post_schema.create, 'body'),
  post_controller.create
);
route.patch(
  '/:id',
  require_login,
  joi_validator(post_schema.update, 'body'),
  post_controller.updateById
);
route.delete('/:id', require_login, post_controller.deleteById);

module.exports = route;
